from math import sqrt
from random import sample
from collections import defaultdict
from random import randrange
from statistics import mean
from itertools import combinations

class Classifier:
    def __init__(self, vectors):
        self.classes = None
        self.classify(vectors)

    def classify(self, vectors):
        newCenter = 0        
        centres = []
        while newCenter is not None:
            centres.append(vectors[newCenter])
            self.replaceCenters(vectors, centres)
            newCenter = self.findNewCenter(vectors)
    
    def findNewCenter(self, vectors):
        result = None
        majorClasses = []
        for center, c in self.classes.items():
            majorClasses.append(max(((i, Classifier.distance(center, vectors[i])) for i in c), key=(lambda x: x[1])))
        majorClass = max(majorClasses, key=(lambda x: x[1]))
        if majorClass[1] > (Classifier.avgCentersDistance(self.classes) / 2):
            result = majorClass[0]
        return result
    
    def replaceCenters(self, vectors, centers):
        classes = defaultdict(list)
        for vector_index, vector in enumerate(vectors):
            center = min(centers, key=lambda x: Classifier.distance(vector, x))
            classes[center].append(vector_index)
        self.classes = classes

    @staticmethod
    def avgCentersDistance(classes):
        centers = list(classes.keys())
        if len(centers) > 1:
            return mean((Classifier.distance(x, y) for x, y in combinations(centers, 2)))
        else:
            return 0

    @staticmethod
    def sqr(a):
        return a*a

    @staticmethod
    def distance(a, b):
        return sqrt(sum(map(Classifier.sqr, (x-y for x, y in zip(a, b)))))

    @staticmethod
    def center(vectors, pointIdxs):
        count = len(vectors[0])
        result = [None]*count
        for vect in range(count):
            result[vect] = sum(vectors[i][vect] for i in pointIdxs) / len(pointIdxs)
        return tuple(result)