from sys import argv, stderr
from plotBuilder import drawResults
from classifier import Classifier
import os
from random import randint

def readData(dataFilePath):
    vectors = None    
    with open(dataFilePath, 'r') as dataFile:
        vectors = []
        for line in dataFile.readlines():
            vectors.append(tuple(map(int, line.split())))
    return vectors


def main():
    count = int(argv[1])
    vectors = []
    for i in range(count):
        vectors.append(tuple(randint(0, 100) for i in range(2)))
    drawResults(vectors, Classifier(vectors).classes)

main()